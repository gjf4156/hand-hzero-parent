#!/usr/bin/env bash

# cicd 函数定义 脚本文件

set -ex # 报错不继续执行

export TEMP_DIR=/cache/${CI_PROJECT_NAME}-${CI_PROJECT_ID}-${CI_COMMIT_REF_NAME/__*/}-front

echo "gitlab-ci -- 缓存目录: $TEMP_DIR"

if [ ! -d "$TEMP_DIR" ] ;then
  mkdir -p $TEMP_DIR
fi

# yarn config set registry http://nexus.saas.hand-china.com/content/groups/hzero-npm-group/

# 安装 node_modules 依赖
function set_build_cache(){

  if  [[ "$CLEAR_TEMP_DIR" =~ "true" && "$IS_INSTALL_STAGE" =~ "true"  ]] ;then
      echo "==: 清空缓存目录：$TEMP_DIR"
      rm -rf $TEMP_DIR
      rm -rf dist* node_modules
      mkdir -p $TEMP_DIR
  fi

  export CURRENT_GIT_HEAD=`git log -1 --pretty=format:"%H"` # 获取当前提交代码版本

  if [ -f "$TEMP_DIR/.commitId" ]; then
    export LAST_BUILD_PARENT_GIT_HEAD=`cat $TEMP_DIR/.commitId` # 获取上一次 build 父项目时的 提交代码版本
    export YARN_LOCK_CHANGE_LOG=`git diff $LAST_BUILD_PARENT_GIT_HEAD $CURRENT_GIT_HEAD --summary --shortstat -- yarn.lock  || echo error` # 对比两次提交版本中的 yarn.lock 是否变化
    if [[ -n "$YARN_LOCK_CHANGE_LOG" && "$IS_INSTALL_STAGE" =~ "true" ]] ;then
      # 如果 yarn.lock 发生变化, 需要更新缓存。
      echo -e "gitlab-ci -- yarn.lock 发生变化, 需要清除之前编译时留下来的缓存。\n\t $YARN_LOCK_CHANGE_LOG"
      export CLEAR_TEMP_DIR=true
      echo "==: yarn.lock 变更, 重新编译"
    else
      echo "==: 执行增量编译"
    fi
  else
      # echo $CURRENT_GIT_HEAD > $TEMP_DIR/.commitId
      export CLEAR_TEMP_DIR=true
      echo "==: 第一次编译（非增量编译）"
  fi

  export HZERO_CICD_INSTALL=false

  if [[ "$CLEAR_TEMP_DIR" == "true" && "x$IS_INSTALL_STAGE" == "xtrue" ]]
    rm -rf ./node_modules
    rm -rf $TEMP_DIR
  fi

  if [[ -f "$TEMP_DIR/node_modules.tar.gz" ]]; then
    tar -zxf $TEMP_DIR/node_modules.tar.gz || (rm -rf node_modules)
  fi

  if  [[  ! -d "node_modules" ]] ;then
      echo "==: 初始化缓存目录：$TEMP_DIR"
      rm -rf $TEMP_DIR
      mkdir -p $TEMP_DIR
      rm -rf ./node_modules
      echo "==: 开始安装依赖"
      yarn install --no-progress --non-interactive --registry http://nexus.saas.hand-china.com/content/groups/hzero-npm-group/
      echo $CURRENT_GIT_HEAD > $TEMP_DIR/.commitId
      node ./node_modules/hzero-cli/bin/hzero-cli.js info
      node ./node_modules/hzero-cli/bin/hzero-cli.js build --build-dll
      tar -zcf $TEMP_DIR/node_modules.tar.gz ./node_modules
      export HZERO_CICD_INSTALL=true
      echo "==: 结束安装依赖"
  fi

  export NODE_PROFILE=production
  # export SKIP_NO_CHANGE_MODULE=true
  node ./node_modules/hzero-cli/bin/hzero-cli.js transpile --transpile-common-if-change --non-interactive

}

# 编译 singleapp
function build_singleapp() {

  # if [ -f "$TEMP_DIR/dist_singleapp.tar.gz" ]; then
  #   tar -zxf $TEMP_DIR/dist_singleapp.tar.gz
  # fi

  # npx cross-env BUILD_DIST_PATH=./dist_singleapp NODE_OPTIONS='--max_old_space_size=8196' NODE_PROFILE=production node ./node_modules/hzero-cli/bin/hzero-cli.js build --only-build-parent # 可独立运行
  npx cross-env BUILD_DIST_PATH=./dist_singleapp NODE_OPTIONS='--max_old_space_size=8196' BUILD_PUBLIC_MS=true BUILD_SINGLE_PUBLIC_MS=true DISABLE_BUILD_DLL=true NODE_PROFILE=production node ./node_modules/hzero-cli/bin/hzero-cli.js build --only-build-parent # 不可独立运行，但是可以被别的主工程通过 PUBLIC_PACKAGE_URL 引用

  rm -rf ./dist
  mv ./dist_singleapp ./dist

}

# 编译 parent
function build_parent() {

  if [ -f "$TEMP_DIR/dist_parent.tar.gz" ]; then
    tar -zxf $TEMP_DIR/dist_parent.tar.gz || (rm -rf ./dist_parent)
  fi

  export HZERO_CICD_BUILD_PARENT=${HZERO_CICD_BUILD_PARENT:-false}

  if [ -f "./dist_parent/.commitId" ] ; then

    export LAST_BUILD_PARENT_GIT_HEAD=`cat ./dist_parent/.commitId` # 获取上一次 build 父项目时的 提交代码版本
    export GIT_DIFF_PAENT=`git diff --stat ${CURRENT_GIT_HEAD} ${LAST_BUILD_PARENT_GIT_HEAD} src`

    if [[  "x$GIT_DIFF_PAENT" != "x" ]] ;then
      echo 父模块发生变化
      export HZERO_CICD_BUILD_PARENT=true
    fi

  else
    export HZERO_CICD_BUILD_PARENT=true
  fi

  if [ "$HZERO_CICD_BUILD_PARENT" == "true" ]; then

    npx cross-env BUILD_DIST_PATH=./dist_parent NODE_OPTIONS='--max_old_space_size=8196' NODE_PROFILE=production node ./node_modules/hzero-cli/bin/hzero-cli.js build --only-build-parent
    echo $CURRENT_GIT_HEAD > ./dist_parent/.commitId
    tar -zcf $TEMP_DIR/dist_parent.tar.gz ./dist_parent

  fi

  rm -rf ./dist
  mv ./dist_parent ./dist

}

# # 编译联邦依赖缓存 mfdeps
# function build_deps() {

#   if [ -f "$TEMP_DIR/dist_mfdeps.tar.gz" ]; then
#     tar -zxf $TEMP_DIR/dist_mfdeps.tar.gz || (rm -rf ./dist_mfdeps)
#   fi

#   export HZERO_CICD_BUILD_MF=${HZERO_CICD_BUILD_MF:-false}

#   if [ -f "./dist_mfdeps/.commitId" ] ; then

#     export LAST_BUILD_PARENT_GIT_HEAD=`cat ./dist_mfdeps/.commitId` # 获取上一次 build 父项目时的 提交代码版本
#     export GIT_DIFF_PAENT=`git diff --stat ${CURRENT_GIT_HEAD} ${LAST_BUILD_PARENT_GIT_HEAD} yarn.lock`

#     if [[ ! -n $GIT_DIFF_PARENT ]] ;then
#       echo 依赖发生变化
#       export HZERO_CICD_BUILD_MF=true
#     fi

#   else
#     export HZERO_CICD_BUILD_MF=true
#   fi

#   # if [ ! -d "./dist_mfdeps/packages/moment" ]; then
#   #   HZERO_CICD_BUILD_MF=true
#   # fi

#   if [ "$HZERO_CICD_BUILD_MF" == "true" ]; then

#     # npx cross-env SKIP_NO_CHANGE_MODULE=true BUILD_DIST_PATH=./dist_parent NODE_OPTIONS='--max_old_space_size=8196' NODE_PROFILE=production node ./node_modules/umi/bin/umi.js hb-cmf
#     npx cross-env SKIP_NO_CHANGE_MODULE=true BUILD_DIST_PATH=./dist_mfdeps NODE_OPTIONS='--max_old_space_size=8196' NODE_PROFILE=production node ./node_modules/umi/bin/umi.js hzero-build-dep --all
#     echo $CURRENT_GIT_HEAD > ./dist_mfdeps/.commitId
#     tar -zcf $TEMP_DIR/dist_mfdeps.tar.gz ./dist_mfdeps

#   fi

#   mv ./dist_mfdeps ./dist

# }

# 分布式编译子模块
function build_packages() {

  export BUILD_PACKAGES=`node .cicd/gitlab_parallel_get_current_packages.js`;
  if [ -z "$BUILD_PACKAGES" ]; then
    exit
  fi

  if [ -f "$TEMP_DIR/dist_packages_${CI_NODE_INDEX:-1}.tar.gz" ]; then
    tar -zxf $TEMP_DIR/dist_packages_${CI_NODE_INDEX:-1}.tar.gz  || (rm -rf ./dist_packages_${CI_NODE_INDEX:-1})
  fi

  # if [ -f "$TEMP_DIR/cache_dist_packages_${CI_NODE_INDEX:-1}.tar.gz" ]; then
  #   tar -zxf $TEMP_DIR/cache_dist_packages_${CI_NODE_INDEX:-1}.tar.gz
  # fi

  # export HZERO_CICD_BUILD_PACKAGES=${HZERO_CICD_BUILD_PACKAGES:false}


  # if [ "$HZERO_CICD_BUILD_PACKAGES" == "true" ]; then

  echo "==: 开始编译:"$1
  npx cross-env SKIP_NO_CHANGE_MODULE=${SKIP_NO_CHANGE_MODULE:-true} BUILD_DIST_PATH=./dist_packages_${CI_NODE_INDEX:-1} NODE_OPTIONS='--max_old_space_size=8196' NODE_PROFILE=production node ./node_modules/hzero-cli/bin/hzero-cli.js build --build-skip-parent --only-build-micro $BUILD_PACKAGES
  tar -zcf $TEMP_DIR/dist_packages_${CI_NODE_INDEX:-1}.tar.gz ./dist_packages_${CI_NODE_INDEX:-1}
  # tar -zcf $TEMP_DIR/dist_packages_${CI_NODE_INDEX:-1}.tar.gz ./dist_packages_${CI_NODE_INDEX:-1}

  # fi

  # mv ./dist_packages ./dist
}

# 合并 dist 文件夹函数
function _merge_packages(){
  if  [ -z "$1" ] || [ -z "$2" ] ; then
    echo "必须传入合并的 dist 文件路径!"
    exit 1
  fi
  export MODULE_NAME=`echo $2 | sed -r "s/^.*${1}_(.*)\.tar\.gz/\1/g"`
  echo "==: 合并:"$MODULE_NAME
  tar -zxf $TEMP_DIR/${1}_$MODULE_NAME.tar.gz
  mkdir -p ./${1}/
  cp -r -n -u ./${1}_$MODULE_NAME/* ./${1}/
  rm -rf ./${1}_$MODULE_NAME
}

# 合并子模块分布式编译结果
function merge_packages() {
  if [ -z "$1" ] ;then
    export DIST_PREFIX=dist_packages
  else
    export DIST_PREFIX=$1
  fi
  export -f _merge_packages
  find $TEMP_DIR -maxdepth 1 -name "${DIST_PREFIX}_*.tar.gz" | xargs -n 1 -I {} bash -c "_merge_packages ${DIST_PREFIX} {}"
  if [ "$DIST_PREFIX" != "dist" ]; then
    rm -rf ./dist
    mv ./${DIST_PREFIX} ./dist
  fi
  node .cicd/refreshMicroConfig.js
  # tar -zcf $TEMP_DIR/${DIST_PREFIX}.tar.gz ./dist
}

# 企业微信机器人通知
function wx_robot_notice () {

  export NOTICE_MSG=${1:-执行成功}
  export WX_ROBOT_TOKEN=${WX_ROBOT_TOKEN:-f9baa7d9-f3c1-437e-a98e-30c74a5c0877}

echo '
 {    "msgtype":
     "markdown" , "markdown": {   "content": "流水线 <font color=\"warning\">['${CI_PROJECT_NAME}']('${CI_JOB_URL}')</font>, '${NOTICE_MSG}'\n
     >修改内容: [<font color=\"comment\">'${CI_COMMIT_MESSAGE}'</font>](https://code.choerodon.com.cn/'${CI_PROJECT_NAMESPACE}'/'${CI_PROJECT_NAME}'/-/commit/'${CI_COMMIT_SHA}') \n
     >提交人: '${CI_COMMIT_AUTHOR}'     \n
     >流水线日志: https://code.choerodon.com.cn/'${CI_PROJECT_NAMESPACE}'/'${CI_PROJECT_NAME}'/-/commit/'${CI_COMMIT_SHA}'
     "  }    }
' | curl "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=${WX_ROBOT_TOKEN}" -H 'Content-Type:: application/json' -d @-

}
