package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_config.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_config") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_config_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_config", remarks: "") {
            column(name: "id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "tenant_id", type: "bigint", remarks: "租户ID") { constraints(nullable: "false") }
            column(name: "unit_id", type: "bigint", remarks: "个性化单元ID") { constraints(nullable: "false") }
            column(name: "unit_title", type: "varchar(" + 96 * weight + ")", remarks: "单元标题")
            column(name: "form_max_col", type: "tinyint", remarks: "表单列最大值")
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "user_id", type: "bigint", defaultValue: -1, remarks: "用户id")
            column(name: "form_page_size", type: "int", remarks: "表格分页大小")
            column(name: "show_lines", type: "int", remarks: "折叠表单显示行数")
            column(name: "show_field", type: "varchar(" + 255 * weight + ")", remarks: "默认展示字段")
            column(name: "filter_sort", type: "varchar(" + 2000 + ")", remarks: "筛选器排序")
            column(name: "default_filter", type: "varchar(" + 60 * weight + ")", remarks: "默认筛选器编码")
            column(name: "sorted_enabled", type: "tinyint", defaultValue: -1, remarks: "排序启用标识，默认开启") { constraints(nullable: "false") }
        }


        addUniqueConstraint(columnNames: "unit_id,tenant_id,user_id", tableName: "hpfm_cusz_config", constraintName: "hpfm_cusz_config_U1")
    }
}