package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_personal_table.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_personal_table") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_personal_table_s', startValue: "1")
        }
        createTable(tableName: "hpfm_personal_table", remarks: "个性化表数据") {
            column(name: "id", type: "bigint", autoIncrement: true, remarks: "主键") { constraints(primaryKey: true) }
            column(name: "user_id", type: "bigint", remarks: "用户id") { constraints(nullable: "false") }
            column(name: "tenant_id", type: "bigint", remarks: "租户ID") { constraints(nullable: "false") }
            column(name: "code", type: "varchar(" + 120 * weight + ")", remarks: "功能编码") { constraints(nullable: "false") }
            column(name: "data_json", type: "longtext", remarks: "数据")
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "创建人") { constraints(nullable: "false") }
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "最近更新人") { constraints(nullable: "false") }
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "创建时间") { constraints(nullable: "false") }
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "最近更新时间") { constraints(nullable: "false") }
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
        }


    }
}