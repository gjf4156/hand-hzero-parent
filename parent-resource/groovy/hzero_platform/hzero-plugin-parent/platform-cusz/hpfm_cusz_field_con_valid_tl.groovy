package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_field_con_valid_tl.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_field_con_valid_tl") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_field_con_valid_tl_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_field_con_valid_tl", remarks: "") {
            column(name: "con_valid_id", type: "bigint", remarks: "hpfm_cusz_field_con_valid 主键") { constraints(nullable: "false") }
            column(name: "lang", type: "varchar(" + 30 * weight + ")", remarks: "语言") { constraints(nullable: "false") }
            column(name: "error_message", type: "varchar(" + 255 * weight + ")", remarks: "")
        }


        addUniqueConstraint(columnNames: "con_valid_id,lang", tableName: "hpfm_cusz_field_con_valid_tl", constraintName: "hpfm_con_valid_tl_U1")
    }
}