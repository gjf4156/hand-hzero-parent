package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_config_field_wdg.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_config_field_wdg") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_config_field_wdg_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_config_field_wdg", remarks: "") {
            column(name: "id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "config_field_id", type: "bigint", remarks: "个性化字段配置表主键") { constraints(nullable: "false") }
            column(name: "tenant_id", type: "bigint", remarks: "租户ID") { constraints(nullable: "false") }
            column(name: "field_widget", type: "varchar(" + 255 * weight + ")", remarks: "字段控件类型")
            column(name: "text_max_length", type: "int", remarks: "TEXT最大程度")
            column(name: "text_min_length", type: "int", remarks: "TEXT最小程度")
            column(name: "text_area_max_line", type: "int", remarks: "文本域组件，最大行数")
            column(name: "source_code", type: "varchar(" + 255 * weight + ")", remarks: "LOV值集或者值集视图编码")
            column(name: "date_format", type: "varchar(" + 60 * weight + ")", remarks: "日期格式")
            column(name: "number_precision", type: "tinyint", remarks: "数值精度")
            column(name: "number_min", type: "int", remarks: "数值最小值")
            column(name: "number_max", type: "bigint", remarks: "数值最大值")
            column(name: "switch_value", type: "varchar(" + 30 * weight + ")", remarks: "开关值")
            column(name: "bucket_name", type: "varchar(" + 255 * weight + ")", remarks: "上传组件，桶名")
            column(name: "bucket_directory", type: "varchar(" + 255 * weight + ")", remarks: "上传组件，桶目录")
            column(name: "link_title", type: "varchar(" + 255 * weight + ")", remarks: "链接标题")
            column(name: "link_href", type: "varchar(" + 510 * weight + ")", remarks: "链接地址")
            column(name: "link_new_window", type: "tinyint", defaultValue: 0, remarks: "是否打开新窗口")
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "default_value", type: "varchar(" + 2000 + ")", remarks: "")
            column(name: "multiple_flag", type: "tinyint", remarks: "是否多选组件")
            column(name: "placeholder", type: "varchar(" + 225 * weight + ")", remarks: "组件placeholder")
            column(name: "help", type: "varchar(" + 255 * weight + ")", remarks: "个性化记录新增，占位提示文本")
            column(name: "pattern", type: "varchar(" + 120 * weight + ")", remarks: "个性化记录新增，正则表达式")
            column(name: "non_strict_step", type: "int", remarks: "个性化记录新增，非严格步距")
            column(name: "step", type: "int", remarks: "个性化记录新增，步长")
            column(name: "thousandth_enabled", type: "tinyint", remarks: "个性化记录新增，是否千分位")
            column(name: "text_area_auto_size", type: "varchar(" + 60 * weight + ")", remarks: "个性化记录新增，是否自适应内容高度：auto、fixed、custom")
            column(name: "text_area_min_rows", type: "int", remarks: "个性化记录新增，内容高度最小展示")
            column(name: "text_area_max_rows", type: "int", remarks: "个性化记录新增，内容高度最大展示")
            column(name: "searchable", type: "tinyint", remarks: "个性化记录新增，是否支持搜索")
            column(name: "model_text_field", type: "varchar(" + 120 * weight + ")", remarks: "个性化记录新增，显示名称存储字段")
            column(name: "upload_name", type: "varchar(" + 120 * weight + ")", remarks: "个性化记录新增，附件显示名")
            column(name: "file_list_max_length", type: "int", remarks: "个性化记录新增，上传文件最大数量")
            column(name: "limit_size", type: "int", remarks: "个性化记录新增，单个文件上传限制")
            column(name: "link_type", type: "varchar(" + 60 * weight + ")", remarks: "链接类型")
            column(name: "modal_width", type: "int", remarks: "弹框宽度")
            column(name: "modal_height", type: "int", remarks: "弹框高度")
            column(name: "upload_show_flag", type: "tinyint", defaultValue: 0, remarks: "上传组件-上传文件列表展开标识")
            column(name: "thousands_flag", type: "tinyint", defaultValue: -1, remarks: "是否启用千分位标识，-1：保持原有逻辑")
            column(name: "incl_now_day_flag", type: "tinyint", remarks: "")
        }


        addUniqueConstraint(columnNames: "config_field_id,tenant_id", tableName: "hpfm_cusz_config_field_wdg", constraintName: "hpfm_cusz_config_field_wdg_U1")
    }

}