package script.db

databaseChangeLog(logicalFilePath: 'script/db/hmde_model_relation_pub.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hmde_model_relation_pub") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hmde_model_relation_pub_s', startValue: "1")
        }
        createTable(tableName: "hmde_model_relation_pub", remarks: "模型关系发布") {
            column(name: "id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "code", type: "varchar(" + 32 * weight + ")", remarks: "关系代码") { constraints(nullable: "false") }
            column(name: "name", type: "varchar(" + 120 * weight + ")", remarks: "关系名称") { constraints(nullable: "false") }
            column(name: "relation_type", type: "varchar(" + 20 * weight + ")", remarks: "关系类型。包括ONE_TO_ONE，ONE_TO_MANY") { constraints(nullable: "false") }
            column(name: "master_model_object_code", type: "varchar(" + 32 * weight + ")", remarks: "主模型代码，hmde_model_object_pub.code") { constraints(nullable: "false") }
            column(name: "relation_model_object_code", type: "varchar(" + 32 * weight + ")", remarks: "关联模型代码，hmde_model_object_pub.code") { constraints(nullable: "false") }
            column(name: "description", type: "varchar(" + 255 * weight + ")", remarks: "关系描述")
            column(name: "tenant_id", type: "bigint", remarks: "租户ID") { constraints(nullable: "false") }
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "") { constraints(nullable: "false") }
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "") { constraints(nullable: "false") }
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "") { constraints(nullable: "false") }
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "") { constraints(nullable: "false") }
        }


        addUniqueConstraint(columnNames: "code", tableName: "hmde_model_relation_pub", constraintName: "hmde_model_relation_pub_u3")
    }
}