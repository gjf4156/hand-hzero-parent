package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_unit_field_tl.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_unit_field_tl") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_unit_field_tl_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_unit_field_tl", remarks: "") {
            column(name: "id", type: "bigint", remarks: "表ID，主键，供其他表做外键") { constraints(nullable: "false") }
            column(name: "lang", type: "varchar(" + 30 * weight + ")", remarks: "语言") { constraints(nullable: "false") }
            column(name: "field_name", type: "varchar(" + 255 * weight + ")", remarks: "多语言字段")
            column(name: "help_message", type: "varchar(" + 1200 * weight + ")", remarks: "帮助信息")
        }


        addUniqueConstraint(columnNames: "id,lang", tableName: "hpfm_cusz_unit_field_tl", constraintName: "hpfm_cusz_unit_field_tl_U1")
    }
}