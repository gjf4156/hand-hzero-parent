package script.db

databaseChangeLog(logicalFilePath: 'script/db/cusz_fix.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-05-20-cusz_fix") {
        sqlFile(path: './cusz_fix.sql', relativeToChangelogFile: true, stripComments: true)
    }
}