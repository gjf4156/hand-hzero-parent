package script.db

databaseChangeLog(logicalFilePath: 'script/db/hmde_mod_rel_field_pub.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hmde_mod_rel_field_pub") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hmde_mod_rel_field_pub_s', startValue: "1")
        }
        createTable(tableName: "hmde_mod_rel_field_pub", remarks: "模型关系关联字段发布") {
            column(name: "id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "relation_id", type: "bigint", remarks: "模型关系ID，hmde_model_relation_pub.id") { constraints(nullable: "false") }
            column(name: "master_model_field_code", type: "varchar(" + 32 * weight + ")", remarks: "主字段代码，hmde_model_field_pub.code") { constraints(nullable: "false") }
            column(name: "relation_model_field_code", type: "varchar(" + 32 * weight + ")", remarks: "关联字段代码，hmde_model_field_pub.code") { constraints(nullable: "false") }
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "") { constraints(nullable: "false") }
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "") { constraints(nullable: "false") }
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "") { constraints(nullable: "false") }
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "") { constraints(nullable: "false") }
        }


        addUniqueConstraint(columnNames: "relation_id,master_model_field_code,relation_model_field_code", tableName: "hmde_mod_rel_field_pub", constraintName: "hmde_mod_rel_field_pub_u1")
    }
}