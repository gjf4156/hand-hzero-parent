package script.db

databaseChangeLog(logicalFilePath: 'script/db/hadm_ds_bdct_target.groovy') {
    changeSet(author: "chihao.ran@hand-china.com", id: "2022-03-08-hadm_ds_bdct_target"){
        def weight = 1
        if(helper.isSqlServer()){
            weight = 2
        } else if(helper.isOracle()){
            weight = 3
        }
        if(helper.dbType().isSupportSequence()){
            createSequence(sequenceName: 'hadm_ds_bdct_target_s', startValue:"1")
        }
        createTable(tableName: "hadm_ds_bdct_target", remarks: "数据同步目标表") {
            column(name: "target_id", type: "bigint",autoIncrement: true,    remarks: "主键")  {constraints(primaryKey: true)} 
            column(name: "source_id", type: "bigint",  remarks: "来源id，hadm_ds_bdct_master.source_id")  {constraints(nullable:"false")}  
            column(name: "host_type", type: "varchar(" + 30* weight + ")",  remarks: "数据源网关类型")  {constraints(nullable:"false")}
            column(name: "data_source_id", type: "bigint",  remarks: "数据源id")  {constraints(nullable:"false")}  
            column(name: "data_source_code", type: "varchar(" + 120* weight + ")",  remarks: "数据源编码")  {constraints(nullable:"false")}
            column(name: "target_db", type: "varchar(" + 60* weight + ")",  remarks: "数据库名称")  {constraints(nullable:"false")}
            column(name: "target_schema", type: "varchar(" + 60* weight + ")",  remarks: "schema名称")
            column(name: "table_name", type: "varchar(" + 60* weight + ")",  remarks: "目标表的表名称")  {constraints(nullable:"false")}
            column(name: "enabled_flag", type: "tinyint",   defaultValue:"1",   remarks: "启用标识，1-启用，0-禁用，默认1")  {constraints(nullable:"false")}  
            column(name: "auto_created_flag", type: "tinyint",   defaultValue:"1",   remarks: "自动创建标识，1-启用，0-禁用，默认1")  {constraints(nullable:"false")}  
            column(name: "remarks", type: "varchar(" + 240* weight + ")",  remarks: "备注")
            column(name: "object_version_number", type: "bigint",   defaultValue:"1",   remarks: "行版本号，用来处理锁")  {constraints(nullable:"false")}
            column(name: "creation_date", type: "datetime",   defaultValueComputed:"CURRENT_TIMESTAMP",   remarks: "")  {constraints(nullable:"false")}
            column(name: "created_by", type: "bigint",   defaultValue:"-1",   remarks: "")  {constraints(nullable:"false")}
            column(name: "last_updated_by", type: "bigint",   defaultValue:"-1",   remarks: "")  {constraints(nullable:"false")}
            column(name: "last_update_date", type: "datetime",   defaultValueComputed:"CURRENT_TIMESTAMP",   remarks: "")  {constraints(nullable:"false")}
        }
        addUniqueConstraint(columnNames:"source_id,data_source_id,target_db,target_schema,table_name",tableName:"hadm_ds_bdct_target",constraintName: "hadm_ds_bdct_target_u1")
    }
}
