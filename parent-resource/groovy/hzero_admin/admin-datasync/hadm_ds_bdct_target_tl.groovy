package script.db

databaseChangeLog(logicalFilePath: 'script/db/hadm_ds_bdct_target_tl.groovy') {
    def weight = 1
    if (helper.isSqlServer()) {
        weight = 2
    } else if (helper.isOracle()) {
        weight = 3
    }
    changeSet(author: "chihao.ran@hand-china.com", id: "2022-03-08-hadm_ds_bdct_target_tl") {
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hadm_ds_bdct_target_tl_s', startValue: "1")
        }
        createTable(tableName: "hadm_ds_bdct_target_tl", remarks: "数据同步广播表多语言表") {
            column(name: "target_id", type: "bigint", remarks: "来源表id") { constraints(nullable: "false") }
            column(name: "lang", type: "varchar(" + 30 * weight + ")", remarks: "语言") { constraints(nullable: "false") }
            column(name: "remarks", type: "varchar(" + 240 * weight + ")", remarks: "备注")
        }
        addUniqueConstraint(columnNames: "target_id,lang", tableName: "hadm_ds_bdct_target_tl", constraintName: "hadm_ds_bdct_target_tl_u1")
    }
}
