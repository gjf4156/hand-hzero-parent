package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_unit.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_unit") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_unit_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_unit", remarks: "") {
            column(name: "id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "tenant_id", type: "bigint", defaultValue: 0, remarks: "租户ID") { constraints(nullable: "false") }
            column(name: "unit_code", type: "varchar(" + 255 * weight + ")", remarks: "个性化模型编码") { constraints(nullable: "false") }
            column(name: "unit_type", type: "varchar(" + 32 * weight + ")", remarks: "单元类型,表单,表格,tab页") { constraints(nullable: "false") }
            column(name: "unit_name", type: "varchar(" + 255 * weight + ")", remarks: "个性化模型名称") { constraints(nullable: "false") }
            column(name: "unit_group_id", type: "bigint", remarks: "所属分组编码") { constraints(nullable: "false") }
            column(name: "model_id", type: "bigint", remarks: "主模型ID") { constraints(nullable: "false") }
            column(name: "combine_code", type: "varchar(" + 255 * weight + ")", remarks: "关联组合业务对象编码")
            column(name: "model_code", type: "varchar(" + 255 * weight + ")", remarks: "关联模型编码")
            column(name: "sql_ids", type: "varchar(" + 300 * weight + ")", remarks: "sql_id集合,逗号分隔")
            column(name: "read_only", type: "tinyint", defaultValue: 0, remarks: "是否只读") { constraints(nullable: "false") }
            column(name: "form_max_col", type: "int", remarks: "表单列最大值")
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "label_col", type: "tinyint", remarks: "label列数")
            column(name: "wrapper_col", type: "tinyint", remarks: "wrapper列数")
            column(name: "enable_flag", type: "tinyint", defaultValue: 1, remarks: "启用标志")
            column(name: "con_related_unit", type: "varchar(" + 500 * weight + ")", remarks: "关联单元编码，多个以逗号隔开")
            column(name: "unit_source", type: "varchar(" + 20 * weight + ")", remarks: "导入时的单元数据来源")
            column(name: "query_related_unit", type: "varchar(" + 120 * weight + ")", remarks: "查询关联单元，暂时默认支持一个")
            column(name: "show_field", type: "varchar(" + 255 * weight + ")", remarks: "默认展示字段")
            column(name: "filter_sort", type: "varchar(" + 2000 + ")", remarks: "筛选器排序")
            column(name: "default_filter", type: "varchar(" + 60 * weight + ")", remarks: "默认筛选器编码")
            column(name: "unit_tag", type: "varchar(" + 120 * weight + ")", remarks: "单元标签，C7N,H0")
            column(name: "sorted_enabled", type: "tinyint", remarks: "排序启用标识，默认关闭")
            column(name: "table_name", type: "varchar(" + 120 * weight + ")", remarks: "表名")
        }

        createIndex(tableName: "hpfm_cusz_unit", indexName: "hpfm_cusz_unit_N1") {
            column(name: "unit_group_id")
            column(name: "tenant_id")
        }

        addUniqueConstraint(columnNames: "unit_code,tenant_id", tableName: "hpfm_cusz_unit", constraintName: "hpfm_cusz_unit_U1")
    }
}