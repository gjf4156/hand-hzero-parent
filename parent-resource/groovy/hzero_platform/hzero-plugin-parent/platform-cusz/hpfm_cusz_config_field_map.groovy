package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_config_field_map.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_config_field_map") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_config_field_map_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_config_field_map", remarks: "") {
            column(name: "id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "tenant_id", type: "bigint", remarks: "租户ID") { constraints(nullable: "false") }
            column(name: "config_field_id", type: "bigint", remarks: "个性化字段ID") { constraints(nullable: "false") }
            column(name: "target_model_code", type: "varchar(" + 255 * weight + ")", remarks: "目标模型编码")
            column(name: "target_field_code", type: "varchar(" + 255 * weight + ")", remarks: "目标模型字段编码")
            column(name: "target_field_id", type: "bigint", defaultValue: -1, remarks: "映射目标字段ID") { constraints(nullable: "false") }
            column(name: "source_model_id", type: "bigint", defaultValue: -1, remarks: "目标字段所属模型") { constraints(nullable: "false") }
            column(name: "source_model_code", type: "varchar(" + 255 * weight + ")", remarks: "来源模型字段编码")
            column(name: "source_field_id", type: "bigint", defaultValue: -1, remarks: "映射源字段ID") { constraints(nullable: "false") }
            column(name: "source_field_code", type: "varchar(" + 255 * weight + ")", remarks: "来源模型字段编码")
            column(name: "source_field_alias", type: "varchar(" + 120 * weight + ")", remarks: "映射源字段别名") { constraints(nullable: "false") }
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
        }


    }
}