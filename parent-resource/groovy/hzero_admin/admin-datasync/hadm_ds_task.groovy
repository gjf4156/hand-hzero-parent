package script.db

databaseChangeLog(logicalFilePath: 'script/db/hadm_ds_task.groovy') {
    changeSet(author: "chihao.ran@hand-china.com", id: "2022-03-08-hadm_ds_task"){
        def weight = 1
        if(helper.isSqlServer()){
            weight = 2
        } else if(helper.isOracle()){
            weight = 3
        }
        if(helper.dbType().isSupportSequence()){
            createSequence(sequenceName: 'hadm_ds_task_s', startValue:"1")
        }
        createTable(tableName: "hadm_ds_task", remarks: "数据同步任务表") {
            column(name: "task_id", type: "bigint",autoIncrement: true,    remarks: "主键")  {constraints(primaryKey: true)} 
            column(name: "task_key", type: "varchar(" + 60* weight + ")",  remarks: "任务唯一建，即与当前任务记录关联的外部系统任务唯一键")  {constraints(nullable:"false")}
            column(name: "task_entity_type", type: "varchar(" + 30* weight + ")",  remarks: "任务类型，值集：HADM.DS.TASK_TYPE")
            column(name: "task_entity_id", type: "bigint",  remarks: "任务实体Id，与任务类型相关的实体")   
            column(name: "task_status", type: "varchar(" + 30* weight + ")",  remarks: "任务状态，值集：HADM.DS.TASK_STATUS")  {constraints(nullable:"false")}
            column(name: "remarks", type: "varchar(" + 240* weight + ")",  remarks: "备注")  {constraints(nullable:"false")}
            column(name: "object_version_number", type: "bigint",   defaultValue:"1",   remarks: "行版本号，用来处理锁")  {constraints(nullable:"false")}
            column(name: "creation_date", type: "datetime",   defaultValueComputed:"CURRENT_TIMESTAMP",   remarks: "")  {constraints(nullable:"false")}
            column(name: "created_by", type: "bigint",   defaultValue:"-1",   remarks: "")  {constraints(nullable:"false")}
            column(name: "last_updated_by", type: "bigint",   defaultValue:"-1",   remarks: "")  {constraints(nullable:"false")}
            column(name: "last_update_date", type: "datetime",   defaultValueComputed:"CURRENT_TIMESTAMP",   remarks: "")  {constraints(nullable:"false")}
        }
    }
}
