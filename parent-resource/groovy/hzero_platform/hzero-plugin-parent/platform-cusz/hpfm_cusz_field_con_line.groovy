package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_field_con_line.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_field_con_line") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_field_con_line_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_field_con_line", remarks: "") {
            column(name: "con_line_id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "con_header_id", type: "bigint", remarks: "条件头表主键") { constraints(nullable: "false") }
            column(name: "tenant_id", type: "bigint", defaultValue: 0, remarks: "租户id") { constraints(nullable: "false") }
            column(name: "con_code", type: "int", remarks: "条件编码") { constraints(nullable: "false") }
            column(name: "source_unit_id", type: "bigint", defaultValue: -1, remarks: "源字段所属单元id") { constraints(nullable: "false") }
            column(name: "source_model_id", type: "bigint", defaultValue: -1, remarks: "源字段所属模型id") { constraints(nullable: "false") }
            column(name: "source_model_code", type: "varchar(" + 255 * weight + ")", remarks: "源模型编码")
            column(name: "source_field_id", type: "bigint", defaultValue: -1, remarks: "源字段id") { constraints(nullable: "false") }
            column(name: "source_field_code", type: "varchar(" + 255 * weight + ")", remarks: "源模型字段编码")
            column(name: "con_expression", type: "varchar(" + 12 * weight + ")", remarks: "条件运算符") { constraints(nullable: "false") }
            column(name: "target_type", type: "varchar(" + 30 * weight + ")", remarks: "目标字段类型，本单元、固定值")
            column(name: "target_unit_id", type: "bigint", remarks: "目标字段所属单元id")
            column(name: "target_model_id", type: "bigint", remarks: "目标字段所属模型id")
            column(name: "target_model_code", type: "varchar(" + 255 * weight + ")", remarks: "目标模型字段编码")
            column(name: "target_field_id", type: "bigint", remarks: "目标字段id")
            column(name: "target_field_code", type: "varchar(" + 255 * weight + ")", remarks: "目标模型字段编码")
            column(name: "target_value", type: "varchar(" + 225 * weight + ")", remarks: "目标字段值")
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "target_value_meaning", type: "varchar(" + 225 * weight + ")", remarks: "值集翻译")
            column(name: "source_type", type: "varchar(" + 225 * weight + ")", defaultValue: "CUZ_UNIT", remarks: "条件行类型，CUZ_UNIT，CTX_TENANT_ID，CTX_ROLE_ID，CTX_USER_ID") { constraints(nullable: "false") }
        }


        addUniqueConstraint(columnNames: "con_header_id,con_code,tenant_id", tableName: "hpfm_cusz_field_con_line", constraintName: "hpfm_cusz_field_con_line_U1")
    }
}