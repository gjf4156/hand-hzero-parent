package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_filter.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_filter") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_filter_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_filter", remarks: "") {
            column(name: "filter_id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "tenant_id", type: "bigint", defaultValue: 0, remarks: "租户ID") { constraints(nullable: "false") }
            column(name: "user_id", type: "bigint", defaultValue: -1, remarks: "用户ID") { constraints(nullable: "false") }
            column(name: "unit_id", type: "bigint", remarks: "单元ID") { constraints(nullable: "false") }
            column(name: "filter_code", type: "varchar(" + 255 * weight + ")", remarks: "筛选器编码") { constraints(nullable: "false") }
            column(name: "filter_name", type: "varchar(" + 255 * weight + ")", remarks: "筛选器名称") { constraints(nullable: "false") }
            column(name: "default_flag", type: "tinyint", defaultValue: 0, remarks: "默认标识") { constraints(nullable: "false") }
            column(name: "enabled_flag", type: "tinyint", defaultValue: 1, remarks: "启用标识")
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "default_sorted_field", type: "varchar(" + 255 * weight + ")", remarks: "默认排序字段别名")
            column(name: "default_sorted_order", type: "varchar(" + 30 * weight + ")", remarks: "默认排序字段默认顺序")
        }

        createIndex(tableName: "hpfm_cusz_filter", indexName: "hpfm_cusz_filter_n1") {
            column(name: "unit_id")
        }

        addUniqueConstraint(columnNames: "filter_code,tenant_id", tableName: "hpfm_cusz_filter", constraintName: "hpfm_cusz_filter_U1")
    }
}