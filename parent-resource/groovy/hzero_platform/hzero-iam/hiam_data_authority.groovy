package script.db
databaseChangeLog(logicalFilePath: 'script/db/hiam_data_authority.groovy') {
    def weight_c = 1
    if(helper.isOracle()){
    weight_c = 3
    }
    if(helper.isSqlServer()){
    weight_c = 2
    }
    changeSet(author: "zhiying.dong@hand-china.com", id: "hiam_data_authority-2022-03-08-version-1"){
        if(helper.dbType().isSupportSequence()){
            createSequence(sequenceName: 'hiam_data_authority_s', startValue:"1")
        }
        createTable(tableName: "hiam_data_authority", remarks: "") {
            column(name: "data_authority_id", type: "bigint",autoIncrement: true,    remarks: "主键")  {constraints(primaryKey: true)} 
            column(name: "service_name", type: "varchar(" + 30* weight_c + ")",  remarks: "服务名称")   
            column(name: "menu_id", type: "bigint",  remarks: "菜单ID")   
            column(name: "sql_id", type: "varchar(" + 240* weight_c + ")",  remarks: "来源数据实体，Mapper ID")   
            column(name: "table_name", type: "varchar(" + 30* weight_c + ")",  remarks: "表名称")  {constraints(nullable:"false")}  
            column(name: "relational_expression", type: "varchar(" + 1200* weight_c + ")",  remarks: "关系表达式")   
            column(name: "tenant_id", type: "bigint",   defaultValue:"0",   remarks: "租户ID")  {constraints(nullable:"false")}  
            column(name: "enabled_flag", type: "tinyint",   defaultValue:"1",   remarks: "启用标识，1启用，0禁用。默认启用")  {constraints(nullable:"false")}  
            column(name: "object_version_number", type: "bigint",   defaultValue:"1",   remarks: "行版本号，用来处理锁")  {constraints(nullable:"false")}  
            column(name: "created_by", type: "bigint",   defaultValue:"-1",   remarks: "创建人")  {constraints(nullable:"false")}  
            column(name: "last_updated_by", type: "bigint",   defaultValue:"-1",   remarks: "最近更新人")  {constraints(nullable:"false")}  
            column(name: "creation_date", type: "datetime",   defaultValueComputed:"CURRENT_TIMESTAMP",   remarks: "创建时间")  {constraints(nullable:"false")}
            column(name: "last_update_date", type: "datetime",   defaultValueComputed:"CURRENT_TIMESTAMP",   remarks: "最近更新时间")  {constraints(nullable:"false")}
        }
        addUniqueConstraint(columnNames:"service_name,menu_id,sql_id,table_name,tenant_id",tableName:"hiam_data_authority",constraintName: "hiam_data_authority_u1")
    }
}
