package script.db

databaseChangeLog(logicalFilePath: 'script/db/hmde_compare_ui_data.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hmde_compare_ui_data") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hmde_compare_ui_data_s', startValue: "1")
        }
        createTable(tableName: "hmde_compare_ui_data", remarks: "比较ui接口异常数据") {
            column(name: "id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "unit_code", type: "varchar(" + 255 * weight + ")", remarks: "单元编码")
            column(name: "ui_type", type: "bigint", remarks: "ui接口类型：CUSTOMIZE、FILTER")
            column(name: "diff_data_path", type: "longtext", remarks: "数据路径")
            column(name: "diff_data_source_value", type: "longtext", remarks: "源数据值")
            column(name: "diff_data_target_value", type: "longtext", remarks: "目标数据值")
            column(name: "diff_info", type: "longtext", remarks: "附加差异描述信息")
            column(name: "source_rt", type: "varchar(" + 255 * weight + ")", remarks: "源接口耗时")
            column(name: "target_rt", type: "varchar(" + 255 * weight + ")", remarks: "目标接口耗时")
            column(name: "tenant_id", type: "bigint", remarks: "租户id")
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "") { constraints(nullable: "false") }
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "") { constraints(nullable: "false") }
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "") { constraints(nullable: "false") }
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "") { constraints(nullable: "false") }
            column(name: "field_code", type: "varchar(" + 255 * weight + ")", remarks: "字段编码")
        }

        createIndex(tableName: "hmde_compare_ui_data", indexName: "hmde_compare_ui_data_n1") {
            column(name: "field_code")
            column(name: "tenant_id")
        }

    }
}