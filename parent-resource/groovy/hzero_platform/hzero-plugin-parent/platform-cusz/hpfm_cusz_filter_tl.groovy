package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_filter_tl.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_filter_tl") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_filter_tl_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_filter_tl", remarks: "") {
            column(name: "filter_id", type: "bigint", remarks: "hpfm_cusz_filter 主键") { constraints(nullable: "false") }
            column(name: "filter_name", type: "varchar(" + 255 * weight + ")", remarks: "筛选器名称") { constraints(nullable: "false") }
            column(name: "lang", type: "varchar(" + 30 * weight + ")", remarks: "语言") { constraints(nullable: "false") }
        }


        addUniqueConstraint(columnNames: "filter_id,lang", tableName: "hpfm_cusz_filter_tl", constraintName: "hpfm_cusz_filter_tl_U1")
    }
}