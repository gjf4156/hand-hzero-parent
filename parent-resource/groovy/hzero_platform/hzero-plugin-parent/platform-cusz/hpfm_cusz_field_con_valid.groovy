package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_field_con_valid.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_field_con_valid") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_field_con_valid_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_field_con_valid", remarks: "") {
            column(name: "con_valid_id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "tenant_id", type: "bigint", defaultValue: 0, remarks: "租户ID") { constraints(nullable: "false") }
            column(name: "con_header_id", type: "bigint", remarks: "条件头表主键id") { constraints(nullable: "false") }
            column(name: "con_expression", type: "varchar(" + 255 * weight + ")", remarks: "条件逻辑表达式") { constraints(nullable: "false") }
            column(name: "error_message", type: "varchar(" + 255 * weight + ")", remarks: "")
            column(name: "con_code", type: "int", remarks: "条件编码") { constraints(nullable: "false") }
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "value", type: "varchar(" + 1200 * weight + ")", remarks: "")
        }


        addUniqueConstraint(columnNames: "con_header_id,con_code,tenant_id", tableName: "hpfm_cusz_field_con_valid", constraintName: "hpfm_cusz_field_con_valid_U1")
    }
}