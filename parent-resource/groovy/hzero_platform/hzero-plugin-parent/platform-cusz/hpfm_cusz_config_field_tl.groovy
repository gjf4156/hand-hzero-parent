package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_config_field_tl.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_config_field_tl") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_config_field_tl_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_config_field_tl", remarks: "") {
            column(name: "config_field_id", type: "bigint", remarks: "") { constraints(nullable: "false") }
            column(name: "lang", type: "varchar(" + 8 * weight + ")", remarks: "") { constraints(nullable: "false") }
            column(name: "field_name", type: "varchar(" + 255 * weight + ")", remarks: "多语言字段")
            column(name: "help_message", type: "varchar(" + 1200 * weight + ")", remarks: "帮助信息")
        }


    }
}