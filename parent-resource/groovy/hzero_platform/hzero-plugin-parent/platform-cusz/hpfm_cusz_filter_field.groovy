package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_filter_field.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_filter_field") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_filter_field_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_filter_field", remarks: "") {
            column(name: "filter_field_id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "tenant_id", type: "bigint", defaultValue: 0, remarks: "租户ID") { constraints(nullable: "false") }
            column(name: "filter_id", type: "bigint", remarks: "hpfm_cusz_filter表主键") { constraints(nullable: "false") }
            column(name: "field_alias", type: "varchar(" + 255 * weight + ")", remarks: "字段别名，对应hpfm_cusz_unit_field field_alias") { constraints(nullable: "false") }
            column(name: "fixed_flag", type: "tinyint", defaultValue: 0, remarks: "固定标识") { constraints(nullable: "false") }
            column(name: "rank", type: "int", defaultValue: 0, remarks: "排列顺序") { constraints(nullable: "false") }
            column(name: "default_value", type: "varchar(" + 255 * weight + ")", remarks: "条件默认值")
            column(name: "merge_flag", type: "tinyint", defaultValue: 0, remarks: "是否聚合标识") { constraints(nullable: "false") }
            column(name: "comparison", type: "varchar(" + 30 * weight + ")", remarks: "比较符，用户级使用")
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "pro_default_flag", type: "tinyint", defaultValue: 0, remarks: "是否开启默认值公式，默认不开启") { constraints(nullable: "false") }
        }


        addUniqueConstraint(columnNames: "filter_id,field_alias,tenant_id", tableName: "hpfm_cusz_filter_field", constraintName: "hpfm_cusz_filter_field_U1")
    }

}