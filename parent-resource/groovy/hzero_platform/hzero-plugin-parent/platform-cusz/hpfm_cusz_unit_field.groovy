package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_unit_field.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_unit_field") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_unit_field_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_unit_field", remarks: "") {
            column(name: "id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "tenant_id", type: "bigint", remarks: "租户ID") { constraints(nullable: "false") }
            column(name: "unit_id", type: "bigint", remarks: "单元ID") { constraints(nullable: "false") }
            column(name: "model_id", type: "bigint", defaultValue: -1, remarks: "模型ID") { constraints(nullable: "false") }
            column(name: "model_code", type: "varchar(" + 255 * weight + ")", remarks: "关联模型编码")
            column(name: "field_id", type: "bigint", defaultValue: -1, remarks: "模型字段ID") { constraints(nullable: "false") }
            column(name: "field_name", type: "varchar(" + 255 * weight + ")", remarks: "")
            column(name: "field_name_type", type: "varchar(" + 30 * weight + ")", remarks: "字段名称的取值类型")
            column(name: "field_alias", type: "varchar(" + 255 * weight + ")", remarks: "字段别名")
            column(name: "field_editable", type: "tinyint", defaultValue: "-1", remarks: "是否可编辑，-1条件控制") { constraints(nullable: "false") }
            column(name: "field_required", type: "tinyint", defaultValue: "-1", remarks: "是否必输，-1条件控制") { constraints(nullable: "false") }
            column(name: "form_col", type: "int", remarks: "表单列序号")
            column(name: "form_row", type: "int", remarks: "表单行序号")
            column(name: "grid_seq", type: "int", remarks: "表格列排序号")
            column(name: "grid_width", type: "int", remarks: "表格列宽度")
            column(name: "grid_fixed", type: "varchar(" + 30 * weight + ")", remarks: "表格冻结配置")
            column(name: "render_options", type: "varchar(" + 30 * weight + ")", defaultValue: "WIDGET", remarks: "渲染方式") { constraints(nullable: "false") }
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "field_visible", type: "tinyint", defaultValue: "-1", remarks: "是否显示字段,-1条件控制") { constraints(nullable: "false") }
            column(name: "label_col", type: "int", remarks: "label列数")
            column(name: "wrapper_col", type: "int", remarks: "wrapper列数")
            column(name: "field_code", type: "varchar(" + 255 * weight + ")", remarks: "字段编码")
            column(name: "col_span", type: "int", remarks: "跨列配置")
            column(name: "default_active", type: "tinyint", defaultValue: 1, remarks: "默认激活控制,标签页/折叠面板单元用")
            column(name: "row_span", type: "int", remarks: "跨行配置")
            column(name: "bind_field", type: "varchar(" + 255 * weight + ")", remarks: "当前字段绑定的字段编码，多个字段使用逗号隔开")
            column(name: "display_field", type: "varchar(" + 120 * weight + ")", remarks: "值集显示字段编码")
            column(name: "value_field", type: "varchar(" + 120 * weight + ")", remarks: "值集值字段编码")
            column(name: "event_code", type: "varchar(" + 255 * weight + ")", remarks: "按钮单元字段事件编码")
            column(name: "where_option", type: "varchar(" + 255 * weight + ")", remarks: "where条件运算符")
            column(name: "aggregation_code", type: "varchar(" + 255 * weight + ")", remarks: "聚合组编码")
            column(name: "aggregation_flag", type: "tinyint", defaultValue: 0, remarks: "聚合组标识") { constraints(nullable: "false") }
            column(name: "merge_flag", type: "tinyint", defaultValue: 0, remarks: "筛选器字段合并标识") { constraints(nullable: "false") }
            column(name: "show_field_flag", type: "tinyint", defaultValue: 0, remarks: "默认展示字段标识") { constraints(nullable: "false") }
            column(name: "sorted_flag", type: "tinyint", defaultValue: 0, remarks: "是否可支持排序标识") { constraints(nullable: "false") }
            column(name: "help_message", type: "varchar(" + 1200 * weight + ")", remarks: "帮助信息")
            column(name: "table_name", type: "varchar(" + 120 * weight + ")", remarks: "表名")
        }

        createIndex(tableName: "hpfm_cusz_unit_field", indexName: "hpfm_cusz_unit_field_index1") {
            column(name: "unit_id")
            column(name: "field_id")
            column(name: "tenant_id")
        }

        addUniqueConstraint(columnNames: "unit_id,field_id,field_code,tenant_id", tableName: "hpfm_cusz_unit_field", constraintName: "hpfm_cusz_unit_field_U1")
    }
}