#!/usr/bin/env bash
if [ ! -f hzero-tool-data-install-1.3.15.jar ]
then
    curl http://nexus.saas.hand-china.com/content/repositories/Hzero-Release/org/hzero/tool/hzero-installer/1.3.15.RELEASE/hzero-installer-1.3.15.RELEASE.jar -o ./hzero-tool-data-install-1.3.15.jar
fi

echo -e "\nHZERO 1.9.RELEASE start init..............\n\n"

echo "Startup tool..."


java -Dfile.encoding=utf-8 -jar hzero-tool-data-install-1.3.15.jar

