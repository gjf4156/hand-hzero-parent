package script.db

databaseChangeLog(logicalFilePath: 'script/db/hpfm_cusz_unit_group.groovy') {
    changeSet(author: "hzero@hand-china.com", id: "2022-04-25-hpfm_cusz_unit_group") {
        def weight = 1
        if (helper.isSqlServer()) {
            weight = 2
        } else if (helper.isOracle()) {
            weight = 3
        }
        if (helper.dbType().isSupportSequence()) {
            createSequence(sequenceName: 'hpfm_cusz_unit_group_s', startValue: "1")
        }
        createTable(tableName: "hpfm_cusz_unit_group", remarks: "") {
            column(name: "unit_group_id", type: "bigint", autoIncrement: true, remarks: "") { constraints(primaryKey: true) }
            column(name: "group_code", type: "varchar(" + 255 * weight + ")", remarks: "组编码") { constraints(nullable: "false") }
            column(name: "group_name", type: "varchar(" + 255 * weight + ")", remarks: "组名称") { constraints(nullable: "false") }
            column(name: "group_menu_code", type: "varchar(" + 255 * weight + ")", remarks: "菜单编码") { constraints(nullable: "false") }
            column(name: "object_version_number", type: "bigint", defaultValue: 1, remarks: "行版本号，用来处理锁") { constraints(nullable: "false") }
            column(name: "created_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "creation_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "last_updated_by", type: "bigint", defaultValue: -1, remarks: "")
            column(name: "last_update_date", type: "datetime", defaultValueComputed: "CURRENT_TIMESTAMP", remarks: "")
            column(name: "enable_flag", type: "tinyint", defaultValue: 1, remarks: "启用标志") { constraints(nullable: "false") }
            column(name: "customize_id", type: "bigint", remarks: "hpfm_customize.customize_id")
        }


        addUniqueConstraint(columnNames: "group_code", tableName: "hpfm_cusz_unit_group", constraintName: "hpfm_cusz_unit_group_U1")
    }
}